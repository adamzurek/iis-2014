<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 29.11.2014
 * Time: 11:46
 */

namespace App\Model;


class EmployeeModel extends BaseModel
{
    const
        TABLE_EMPLOYEE = 'zamestnanec',
        TABLE_EDUCATED = 'vyskoleni';


    /**
     * Informace o konkrétního zaměstnance.
     *
     * @param int $rc
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getEmployee ($rc)
    {
        return $this->db->table(self::TABLE_EMPLOYEE)->wherePrimary($rc)->fetch();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getEmployees ()
    {
        return $this->db->table(self::TABLE_EMPLOYEE);
    }
} 