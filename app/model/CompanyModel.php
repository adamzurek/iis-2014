<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 17:44
 */

namespace App\Model;

use \Nette\Utils\ArrayHash;

class CompanyModel extends BaseModel
{
    const
        TABLE_COMPANY = 'firma';

    /**
     * Informace o konkrétní firmě.
     *
     * @param int $id
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getCompany ($id)
    {
        return $this->db->table(self::TABLE_COMPANY)->wherePrimary($id)->fetch();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getCompanies ()
    {
        return $this->db->table(self::TABLE_COMPANY);
    }

    /**
     * Vloží novou firmu
     *
     * @param \Nette\Utils\ArrayHash $data
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function addCompany (ArrayHash $data)
    {
        $data->heslo = sha1($data->heslo);
        $this->db->table(self::TABLE_COMPANY)->insert($data);

        // moc velký ičo tak si to musim hledat sám
        return $this->db->table(self::TABLE_COMPANY)->wherePrimary($data->ico)->fetch();
    }
}