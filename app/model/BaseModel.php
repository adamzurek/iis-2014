<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 1. 11. 2014
 * Time: 14:14
 */

namespace App\Model;


class BaseModel extends \Nette\Object
{
    /**
     * @var \Nette\Database\Context
     */
    protected $db;

    public function __construct (\Nette\Database\Context $db)
    {
        $this->db = $db;
    }
}
