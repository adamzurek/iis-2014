<?php

namespace App\Model;

class UserAuthorizator extends \Nette\Security\Permission implements \Nette\Security\IAuthorizator
{
    public function __construct() {
        $logged = array('firma', 'stredisko', 'ucastnik', 'zamestnanec',);

        parent::addRole('firma');
        parent::addRole('guest');
        parent::addRole('stredisko');
        parent::addRole('ucastnik');
        parent::addRole('zamestnanec');

        parent::addResource('Attendee');
        parent::addResource('Course');
        parent::addResource('Company');
        parent::addResource('Employee');
        parent::addResource('Homepage');
        parent::addResource('Lecture');
        parent::addResource('Room');
        parent::addResource('Sign');

        parent::allow('guest', 'Attendee', 'register'); // allow Company:register
        parent::allow('guest', 'Course', 'list');       // allow Course:list
        parent::allow('guest', 'Company', 'register');  // allow Company:register
        parent::allow('guest', 'Homepage', 'default');	// allow Homepage:default
        parent::allow('guest', 'Sign', 'in');	        // allow Sign:in

        parent::allow('firma', 'Course', 'order');      // allow Course:order
        parent::allow('firma', 'Course', 'list');       // allow Course:list
        parent::allow('firma', 'Course', 'listCompanyOrdered');  // allow Course:listCompanyOrdered

        parent::allow('stredisko', 'Course', 'add');    // allow Course:add
        parent::allow('stredisko', 'Course', 'certify');// allow Course:edit
        parent::allow('stredisko', 'Course', 'edit');   // allow Course:edit
        parent::allow('stredisko', 'Course', 'list');   // allow Course:list
        parent::allow('stredisko', 'Room', 'add');      // allow Course:edit

        parent::allow('ucastnik', 'Course', 'listOrdered');     // allow Course:listOrdered

        parent::allow('zamestnanec', 'Course', 'listOrdered');  // allow Course:listOrdered
        parent::allow('zamestnanec', 'Course', 'editOrdered');  // allow Course:editOrdered

        parent::allow($logged, 'Attendee', 'edit');     // allow Attendee:edit
        parent::allow($logged, 'Lecture', 'list');      // allow Lecture:list
        parent::allow($logged, 'Sign', 'out');          // allow Sign:out
    }
}
