<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 1. 11. 2014
 * Time: 14:13
 */

namespace App\Model;


use Nette\Utils\ArrayHash;

class CourseModel extends BaseModel
{

    const
        TABLE_COURSE = 'provadene_kurzy',
        TABLE_ORDERED_COURSE = 'objednane_kurzy',
        TABLE_LECTURE = 'lekce',
        TABLE_ORDERED_LECTURE = 'objednane_lekce',
        TABLE_PREREQUISITE = 'prerekvizity',
        TABLE_ENROLLED = 'prihlaseni';

    /**
     * Informace o konkrétním prováděném kurzu.
     *
     * @param int $id
     *
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getCourse ($id)
    {
        return $this->db->table(self::TABLE_COURSE)->wherePrimary($id)->fetch();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getCourses ()
    {
        return $this->db->table(self::TABLE_COURSE);
    }

    /**
     * Informace o konkrétních prerekvizitách kurzu.
     *
     * @param int $course_id
     *
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getPrerequisites ($course_id)
    {
        //return $this->db->table(self::TABLE_PREREQUISITE)->wherePrimary($id)->fetch();
        return $this->db->query("SELECT p.kurz, k.nazev, k.id FROM prerekvizity p INNER JOIN provadene_kurzy k ON p.kurz = k.id WHERE p.kurz = ?", $course_id);
    }

    /**
     * @param \Nette\Utils\ArrayHash $values
     *
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function enroll (ArrayHash $values)
    {
        $this->db->table(self::TABLE_ENROLLED)->insert($values);

        return $this->db->table(self::TABLE_ENROLLED)->where($values)->fetch();
    }
}
