<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{

    const COLUMN_PASSWORD_HASH = 'heslo';

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($id, $password, $role) = $credentials;

		$row = $this->database->table($role)->where($role == 'firma' ? 'ico' : 'rc', $id)->fetch();

		if (!$row) {
			throw new Nette\Security\AuthenticationException(($role == 'firma' ? 'IČO' : 'RČ') . ' nenalezeno v databázi', self::IDENTITY_NOT_FOUND);
		} elseif (sha1($password) != $row[self::COLUMN_PASSWORD_HASH]) {
			throw new Nette\Security\AuthenticationException('Zadáno špatné heslo.', self::INVALID_CREDENTIAL);
		} elseif($row['aktivni'] != 1) {
            throw new Nette\Security\AuthenticationException('Účet není aktivní', self::IDENTITY_NOT_FOUND);
        }

		$arr = array(
            'rc'    => $role == 'firma' ? null : $row->rc,
            'ico'   => $role == 'firma' ? $row->ico : ($role == 'ucastnik' ? $row->zamestnan : null),
        );
		unset($arr[self::COLUMN_PASSWORD_HASH]);

		return new Nette\Security\Identity($id, $role, $arr);
	}


	/**
	 * Adds new user.
     *
	 * @param int $id
	 * @param string $password
     * @param string $role
     *
	 * @return void
	 */
	public function add($id, $password, $role)
	{
		$this->database->table($role)->insert(array(
            ($role == 'firma' ? 'ico' : 'rc') => $id,
			self::COLUMN_PASSWORD_HASH => sha1($password),
            'aktivni'   => 1,
		));
	}

}
