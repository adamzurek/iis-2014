<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 18:06
 */

namespace App\Model;

use \Nette\Utils\ArrayHash;

class AttendeeModel extends BaseModel
{
    const
        TABLE_ATTENDEE = 'ucastnik';

    /**
     * Informace o konkrétním účasníkovi.
     *
     * @param int $id
     *
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getAttendee ($id)
    {
        return $this->db->table(self::TABLE_ATTENDEE)->wherePrimary($id)->fetch();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getAttendees ()
    {
        return $this->db->table(self::TABLE_ATTENDEE);
    }

    /**
     * Vloží nového účastníka
     *
     * @param \Nette\Utils\ArrayHash $data
     *
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function addAttendee (ArrayHash $data)
    {
        if ($data->nezamestnan) {
            unset($data->zamestnan);
        }
        unset($data->nezamestnan);

        $data->aktivni = 1;

        $data->heslo = sha1($data->heslo);

        $this->db->table(self::TABLE_ATTENDEE)->insert($data);

        // protože to je velký číslo, tak si musim sám ověřit jestli se vložil řádek -_-
        return $this->db->table(self::TABLE_ATTENDEE)->wherePrimary($data->rc)->fetch();
    }
}
