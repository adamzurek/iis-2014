<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 15:03
 */

namespace App\Model;


class LectureModel extends BaseModel
{

    const
        TABLE_LECTURE = 'lekce';

    /**
     * Informace o konkrétní lekci.
     *
     * @param int $id
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getLecture ($id)
    {
        return $this->db->table(self::TABLE_LECTURE)->wherePrimary($id)->fetch();
    }


    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getLectures ($course_id = null)
    {
        $lectures = $this->db->table(self::TABLE_LECTURE);

        if ($course_id) {
            $lectures = $lectures->where('kurz', $course_id);
        }

        return $lectures;
    }
}
