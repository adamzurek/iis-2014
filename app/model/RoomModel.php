<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 8. 11. 2014
 * Time: 8:11
 */

namespace App\Model;

use \Nette\Database\SqlLiteral;

class RoomModel extends BaseModel
{
    const
        TABLE_ROOM = 'mistnost';

    /**
     * Informace o všech místnostech
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getRooms ()
    {
        return $this->db->table(self::TABLE_ROOM);
    }


    /**
     * Informace o místnosti
     *
     * @param int $id
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getRoom ($id)
    {
        return $this->db->table(self::TABLE_ROOM)->wherePrimary($id)->fetch();
    }

    /**
     * Informace o všech objednaných kurzech konaných v dané místnosti.
     *
     * @param int $room_id
     * @return \Nette\Database\Table\Selection|false
     */
    public function getTimetable ($room_id)
    {
        return $this->db
            ->table(self::TABLE_ROOM)
            ->wherePrimary($room_id)
            ->order(':objednane_kurzy:objednane_lekce.datum DESC')
            ->fetch();
    }
}
