<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

    public function actionSnippet() {
        $this->template->rand = rand(0, 9999);
    }

    public function handleRand ()
    {
        $this->redrawControl('s');
    }

}
