<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 31. 10. 2014
 * Time: 11:15
 */

namespace App\Presenters;

use Nette,
    App\Components\BootstrapForm as Form;


/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{
    /**
     * @persistent
     */
    public $backlink = '';


    /**
     * Sign-in form factory.
     * @return \App\AdminModule\Components\AdminForm
     */
    protected function createComponentSignInForm()
    {
        $form = new Form;

        $form->addText('username', 'Uživatelské jméno')
            ->setRequired('Vložte uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Vložte heslo.');

        $form->addSelect('role', 'Role')
            ->setRequired('Zvolte způsob přihlášení.')
            ->setItems(array(
                'firma'          => 'Firma',
                'ucastnik'       => 'Účastník',
                'zamestnanec'    => 'Zaměstnanec',
                'stredisko'      => 'Středisko',
            ));

        $form->addSubmit('send', 'Přihlásit');

        // call method signInFormSucceeded() on success
        $form->onSuccess[] = $this->signInFormSucceeded;

        return $form;
    }


    /**
     * Zpracování přihlašovacího formuláře
     *
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function signInFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
    {
        /**
         * nastavení expirace přihlášení
         */
        $this->getUser()->setExpiration('20 minutes', TRUE);

        try {
            $this->getUser()->login($values->username, $values->password, $values->role);

            $this->restoreRequest($this->backlink);
            $this->redirect('Homepage:');

        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

    /**
     * Přihlášeného uživatele rovnou pošleme dále
     * @only_guest
     */
    public function actionIn() {
        if ($this->user->loggedIn) {
            $this->restoreRequest($this->backlink);

            $this->redirect('Homepage:');
        }
    }

    /**
     * Odhlášení uivatele
     * @logged
     */
    public function actionOut()
    {
        $this->user->logout();

        $this->flashMessage('Odhlášeno.');

        $this->redirect('in');
    }
}
