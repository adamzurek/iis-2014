<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 1. 11. 2014
 * Time: 14:29
 */

namespace App\Presenters;

class CoursePresenter extends BasePresenter
{

    /**
     * @inject
     * @var \App\Model\CourseModel
     */
    public $model;

    public function actionList ()
    {
        $this->template->courses = $this->model->getCourses();
    }
}
