<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 8. 11. 2014
 * Time: 8:05
 */

namespace App\Presenters;

class RoomPresenter extends BasePresenter
{

    /**
     * @inject
     * @var \App\Model\RoomModel
     */
    public $model;
    /**
     * @param int $id
     */
    public function actionTimetable ($id)
    {
        $room = $this->model->getRoom($id);
        if (!$room) {
            $this->redirect(301, 'list');
        }

        $this->template->room = $this->model->getTimetable($id);
    }

    public function actionList ()
    {
        $this->template->rooms = $this->model->getRooms();
    }
}
