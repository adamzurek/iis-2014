<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 19:22
 */


namespace App\Presenters;

use \App\Components\BootstrapForm as Form,
    \Nette\Utils\ArrayHash;


class AttendeePresenter extends BasePresenter
{
    /**
     * @inject
     * @var \App\Model\AttendeeModel
     */
    public $model;

    /**
     * @inject
     * @var \App\Model\CompanyModel
     */
    public $companyModel;

    public function actionRegister ()
    {
        $this['registerForm']['zamestnan']->setItems($this->companyModel->getCompanies()->fetchPairs('ico', 'nazev'));
    }

    /**
     * @return \App\Components\BootstrapForm
     */
    private function AttendeeForm ()
    {
        $form = new Form();

        $form->addText('jmeno', 'Jméno a příjmení:')
            ->setRequired('Vložte své jméno a příjmení.');

        $form->addText('rc', 'Rodné číslo:')
            ->setRequired('Vložte své rodné číslo.')
            ->setType('number')
            ->addRule(Form::INTEGER, 'Rodné číslo musí být celé číslo.')
            ->addRule(FORM::MAX_LENGTH, 'Rodné číslo musí mít 10 čísel', 10)
            ->addRule(FORM::MIN_LENGTH, 'Rodné číslo musí mít 10 čísel', 10);

        $form->addText('adresa', 'Adresa:')
            ->setRequired('Vložte svou aktuální adresu.');

        $form->addSelect('zamestnan', 'Firma:');

        $form->addCheckbox('nezamestnan', 'Nezaměstnaný ani v jedné z výše zmíněných.');

        $form->addPassword('heslo', 'Heslo:')
            ->setRequired('Prosím, zadejte heslo.');

        $form->addPassword('confirm_password', 'Znovu heslo: ')
            ->setOmitted()
            ->setRequired('Prosím, zadejte znovu heslo pro potvrzení heslo.')
            ->addRule(Form::EQUAL, 'Hesla se musí shodovat!', $form['heslo']);

        $form->onValidate[] = $this->registerFormValidate;

        $form->addSubmit('submit', 'Registrovat');

        return $form;
    }

    /**
     * @return \App\Components\BootstrapForm
     */
    public function createComponentRegisterForm ()
    {
        $form = $this->AttendeeForm();

        $form->onSuccess[] = $this->registerFormSuccess;

        return $form;
    }

    /**
     * @param \App\Components\BootstrapForm $form
     * @param \Nette\Utils\ArrayHash        $values
     */
    public function registerFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->model->addAttendee($values);
        if ($ok) {
            $this->flashMessage('Registrace proběhla úspěšně.', 'success');
            $this->redirect('Course:list');
        } else {
            $this->flashMessage('Registrace selhala.', 'danger');
        }
    }

    /**
     * @param \App\Components\BootstrapForm $form
     *
     * @return bool
     */
    public function registerFormValidate (Form $form)
    {
        $values = $form->getValues();

        $human = $this->model->getAttendee($values->rc);

        if (fmod($values->rc, 11) != 0) {
            $form['rc']->addError('Rodné číslo je neplatné. Zkontrolujte si jeho správnost.');

            return false;
        }

        if ($human) {
            $form['rc']->addError('Účastník s rodným číslem: ' . $human->rc . ' je u nás již registrován.');

            return false;
        }

        return true;
    }
}
