<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 23. 10. 2014
 * Time: 07:07
 */

namespace App\Presenters;

use Nette,
    App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    /**
     * @inject
     * @var \App\Components\IMenuControlFactory
     */
    public $menuControlFactory;

    /**
     * @return \App\Components\MenuControl
     */
    public function createComponentMenu ()
    {
        return $this->menuControlFactory->create();
    }

    public function beforeRender ()
    {
        $this->template->siteName = $this->context->parameters['siteName'];

        $this['menu']
            ->addItem('Course:list', 'Kurzy')
            ->addItem('Company:register', 'Registrace firmy')
            ->addItem('Attendee:register', 'Registrace účastníka')
            ->addItem('Sign:in', 'Administrace');
    }

    /**
     * @inject
     * @var \WebLoader\Nette\LoaderFactory
     */
    public $webLoader;

    /** @return \WebLoader\Nette\CssLoader */
    protected function createComponentCss ()
    {
        return $this->webLoader->createCssLoader('default');
    }

    /** @return \WebLoader\Nette\JavaScriptLoader */
    protected function createComponentJs ()
    {
        return $this->webLoader->createJavaScriptLoader('default');
    }

    /**
     * @var $element
     * @throws Nette\Application\ForbiddenRequestException
     */
    public function checkRequirements ($element)
    {
        $class = $element->name;
        $method = 'action' . $this->presenter->action;

        $signal = $this->presenter->getSignal();
        if ($signal) {
            if ($signal[0]) {
                $class = $this->presenter->getComponent($signal[0]);
            }
            $method = 'handle' . $signal[1];
        }

        try {
            $c = new \Nette\Reflection\Method($class, $method);
            //$c = $element;

            $fail = false;

            if ($c->hasAnnotation('logged')) {
                if (!$this->presenter->user->loggedIn) {
                    $this->flashMessage('Pro tuto akci musíte být přihlášen(a).', 'info');
                    $fail = true;
                }
            } elseif ($c->hasAnnotation('only_guest')) {
                if ($this->presenter->user->loggedIn) {
                    $this->flashMessage('Pro tuto akci nesmíte být přihlášen(a).', 'warning');
                    $fail = true;
                }
            }
            if ($c->hasAnnotation('Allowed')) {
                $allowed = $c->getAnnotation('Allowed');

                $fail = !$this->user->isAllowed($allowed['resource'], $allowed['privilege']);

                if ($fail) {
                    $this->flashMessage('Pro tuto akci nemáte oprávnění.', 'danger');
                }
            }

            if ($fail) {
                throw new Nette\Application\ForbiddenRequestException('K této akci nemáte oprávnění.');
            }
        } catch (\ReflectionException $e) {

        }
    }
}
