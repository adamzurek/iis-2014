<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 17:43
 */

namespace App\Presenters;


use \App\Components\BootstrapForm as Form;
use \Nette\Utils\ArrayHash;

class CompanyPresenter extends BasePresenter
{
    /**
     * @inject
     * @var \App\Model\CompanyModel
     */
    public $model;

    /**
     * @return \App\Components\BootstrapForm
     */
    private function CompanyForm ()
    {
        $form = new Form();

        $form->addText('ico', 'IČO:')
            ->setRequired('Vložte IČO firmy.')
            ->setType('number')
            ->addRule(Form::INTEGER, 'IČO musí být celé číslo.')
            ->addRule(Form::MAX_LENGTH, 'IČO musí mít 8 čísel', 8)
            ->addRule(Form::MIN_LENGTH, 'IČO musí mít 8 čísel', 8);

        $form->addText('nazev', 'Název firmy:')
            ->setRequired('Vložte název firmy.');


        $form->addPassword('heslo', 'Heslo:')
            ->setRequired('Prosím, zadejte heslo.');

        $form->addPassword('confirm_password', 'Znovu heslo: ')
            ->setOmitted()
            ->setRequired('Prosím, zadejte znovu heslo pro potvryení heslo.')
            ->addRule(Form::EQUAL, 'Hesla se musí shodovat!', $form['heslo']);

        $form->onValidate[] = $this->registerFormValidate;

        $form->addSubmit('submit', 'Registrovat');

        return $form;
    }

    /**
     * @return \App\Components\BootstrapForm
     */
    public function createComponentRegisterForm ()
    {
        $form = $this->CompanyForm();

        $form->onSuccess[] = $this->registerFormSuccess;

        return $form;
    }

    /**
     * @param \App\Components\BootstrapForm $form
     * @param \Nette\Utils\ArrayHash        $values
     */
    public function registerFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->model->addCompany($values);
        if ($ok) {
            $this->flashMessage('Vložení záznamu proběhlo úspěšně.', 'success');
            $this->redirect('Course:list:');
        } else {
            $this->flashMessage('Vložení záznamu selhalo.', 'danger');
        }
    }

    /**
     * @param \App\Components\BootstrapForm
     *
     * @return mixed
     */
    public function registerFormValidate (Form $form)
    {
        $values = $form->getValues();

        $company = $this->model->getCompany($values->ico);

        if ($company) {
            $form['ico']->addError('Firma s IČO: ' . $company->ico . ' je u nás již registrována.');

            return false;
        }

        return true;
    }

} 