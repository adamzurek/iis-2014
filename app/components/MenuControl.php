<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 8. 11. 2014
 * Time: 8:40
 */

namespace App\Components;


class MenuControl extends \Nette\Application\UI\Control
{

    const TYPE_MAIN  = ':main',
          TYPE_RIGHT = ':right',
          TYPE_SUB   = ':sub';

    /**
     * @var array
     */
    private $menu = array(
        self::TYPE_MAIN => array(),
        self::TYPE_RIGHT => array(),
        self::TYPE_SUB => array(),
    );

    /**
     * @var string
     */
    public $brand = 'Domů';

    public function __construct ()
    {
        parent::__construct();
    }

    public function render ()
    {
        $this->template->brand = $this->brand;

        $this->template->menu = $this->menu[self::TYPE_MAIN];
        $this->template->submenu = $this->menu[self::TYPE_SUB];
        $this->template->right = $this->menu[self::TYPE_RIGHT];

        $this->template->setFile(__DIR__ . '/../templates/components/menu.latte');
        $this->template->render();
    }

    /**
     * Přidá nový odkaz do menu
     *
     * @param string $link
     * @param string $text
     * @param array  $args
     * @param string $type
     *
     * @return $this
     */
    public function addItem ($link, $text, $type = self::TYPE_MAIN)
    {
        list($presenter, $action) = explode(':', $link, 2);

        if ($this->presenter->user->isAllowed($presenter, $action)) {
            $this->menu[$type][] = (object)array (
                'link' => $this->presenter->link($link),
                'text' => $text,
            );
        }

        return $this;
    }
}

interface IMenuControlFactory
{
    /**
     * @return \App\Components\MenuControl
     */
    public function create();
}
