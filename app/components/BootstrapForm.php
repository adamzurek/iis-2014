<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 31. 10. 2014
 * Time: 11:23
 */

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    Nette\Forms\Controls,
    Nextras\Forms\Rendering\Bs3FormRenderer;

class BootstrapForm extends Form
{

    public function __construct (Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        $renderer = new Bs3FormRenderer();

        $this->setRenderer($renderer);
    }
}
