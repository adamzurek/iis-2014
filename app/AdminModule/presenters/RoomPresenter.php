<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 8. 12. 2014
 * Time: 22:59
 */

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\AdminForm as Form;
use Nette\Utils\ArrayHash;

class RoomPresenter extends BasePresenter
{

    /**
     * @inject
     * @var \App\AdminModule\Model\RoomModel
     */
    public $model;

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentAddForm ()
    {
        $form = new Form;

        $form->addText('kapacita', 'Kapacita')
            ->setType('number')
            ->addRule(Form::INTEGER, 'Musí být celé číslo.')
            ->addRule(Form::MIN, 'Musí být větší než 0', 0)
            ->setRequired();

        $form->addText('cena', 'Cena')
            ->setRequired()
            ->setType('number')
            ->addRule(Form::MIN, 'Musí být větší než 0', 0);

        $form->addText('adresa', 'Adresa')
            ->setRequired()
            ->addRule(Form::MAX_LENGTH, 'Musí být kratší než 100 znaků.', 100);

        $form->addSubmit('add', 'Přidat');

        $form->onSuccess[] = $this->addFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function addFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->model->addRoom($values);
        if ($ok) {
            $this->flashMessage('Přidání místnosti úspěšné.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Přidání místnosti selhalo', 'danger');
        }
    }
}
