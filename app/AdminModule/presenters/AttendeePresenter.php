<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 8. 12. 2014
 * Time: 15:16
 */

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\AdminForm as Form,
    Nette\Utils\ArrayHash;

class AttendeePresenter extends BasePresenter
{
    /**
     * @inject
     * @var \App\AdminModule\Model\AttendeeModel
     */
    public $model;

    /**
     * @inject
     * @var \App\AdminModule\Model\CompanyModel
     */
    public $companyModel;

    /**
     * @Allowed(resource=Attendee, privilege=edit)
     */
    public function actionEdit ()
    {
        if ($this->user->identity->roles[0] == 'ucastnik') {
            $this['editForm']['zamestnan']
                ->setItems(array (0 => 'Žádná firma') + $this->companyModel->getCompanies()->fetchPairs('ico', 'nazev'))
                ->setDefaultValue((int)$this->user->identity->data['ico']);
        }
        $this['editForm']->setDefaults($row = $this->model->get($this->user->identity->roles[0], $this->user->id));
        $this['editForm']['heslo']->setDefaultValue('');
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentEditForm ()
    {
        $form = new Form;

        $form->addText('jmeno', 'Jméno')
            ->setRequired();

        if ($this->user->identity->roles[0] == 'ucastnik') {
            $form->addText('adresa', 'Adresa');

            $form->addSelect('zamestnan', 'Firma');
        }

        $form->addText('heslo', 'Heslo')
            ->setOption('description', 'Pokud nevyplníte tak zůstane původní.');

        $form->addCheckbox('aktivni', 'Aktivní účet')
            ->setRequired();

        $form->addSubmit('edit', 'Upravit');

        $form->onSuccess[] = $this->editFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function editFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->model->editAttendee($this->user->identity->roles[0], $this->user->id, $values);
        if ($ok) {
            $this->flashMessage('Editace úspěšná', 'success');
            $this->redirect('Homepage:');
        } else {
            $this->flashMessage('Editace selhala.', 'danger');
        }
    }
}
