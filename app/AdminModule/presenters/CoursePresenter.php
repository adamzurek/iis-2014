<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Jaroslav Prokop
 * Date: 31. 10. 2014
 * Time: 14:57
 */

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\AdminForm as Form;
use Nette\Utils\ArrayHash;

class CoursePresenter extends BasePresenter
{

    /**
     * @inject
     * @var \App\AdminModule\Model\AttendeeModel
     */
    public $attendeeModel;

    /**
     * @inject
     * @var \App\AdminModule\Model\RoomModel
     */
    public $roomModel;

    /**
     * @inject
     * @var \App\AdminModule\Model\CourseModel
     */
    public $model;

    /**
     * @inject
     * @var \App\AdminModule\Model\CompanyModel
     */
    public $companyModel;

    /**
     * @inject
     * @var \App\AdminModule\Model\LectorModel
     */
    public $lectorModel;

    public function beforeRender ()
    {
        parent::beforeRender();

        $this['menu']
            ->addItem('Course:listCompanyOrdered', 'Objednané kurzy', $this['menu']::TYPE_SUB)
            ->addItem('Course:add', 'Přidat nový kurz', $this['menu']::TYPE_SUB);
    }


    /**
     * @param int $id
     */
    public function actionCertify ($id)
    {
        $this->template->course = $this->model->getCourse($id);

        $this->template->attendee = $this->attendeeModel->get($this->user->identity->roles[0], $this->user->id);
    }

    /**
     * Seznam všech prováděných kurzů
     */
    public function actionList ()
    {
        $this->template->courses = $this->model->getCourses();
    }

    /**
     * Seznam všech objednaných kurzů (veřejných, nebo objednaných vlastní firmou)
     *
     * @param int $id - id prováděného kurzu
     *
     * @Allowed(resource=Course, privilege=listOrdered)
     */
    public function actionListOrdered ($id)
    {
        $this->template->course_c = $this->model->getCourse($id);

        if ($this->user->identity->roles[0] == 'zamestnanec') {
            $this->template->courses = $this->model->getOrderedCourses($id, -1);
        } else {
            $this->template->courses = $this->model->getOrderedCourses($id, $this->user->identity->data['ico']);
        }
    }

    /**
     * Zobrazí objednané lekce přihlášené firmy
     *
     * @Allowed(resource=Course, privilege=listCompanyOrdered)
     */
    public function actionListCompanyOrdered ()
    {
        $this->template->company = $this->companyModel->getCompany($this->user->identity->data['ico']);

        $this->template->courses = $this->model->getOrderedCoursesByCompany($this->user->identity->data['ico']);
    }

    /**
     * @param int $id
     */
    public function actionListAttendees ($id)
    {
        $this->template->course = $this->model->getCourse($id);

        $this->template->attendees = $this->attendeeModel->getAttendeesByCourse($id);
    }

    /**
     * @param int $id id kurzu
     */
    public function actionEdit ($id)
    {
        $defaults = $this->model->getCourse($id);
        if (!$defaults) {
            $this->flashMessage('Záznam nebyl nalezen', 'danger');
            $this->redirect('list');
        }

        $prerek = $this->model->getPrerequisites($id);

        $this['editForm']->setDefaults($defaults->toArray());
        $this['editForm']['prerek']->setDefaultValue($prerek->fetchPairs('nazev', 'id'));
    }

    /**
     * @param int $id id kurzu
     */
    public function actionOrder ($id)
    {
        $defaults = $this->model->getCourse($id);
        if (!$defaults) {
            $this->flashMessage('Záznam nebyl nalezen', 'danger');
            $this->redirect('list');
        }

        $this['orderForm']->setDefaults($defaults->toArray());
        $this['orderForm']['kurz']->setValue($id);
        $this['orderForm']['vede']->setItems($this->lectorModel->getLectors($id)->fetchPairs('rc', 'jmeno'));
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentEditForm ()
    {
        $form = $this->CourseForm();

        $form->addHidden('id');

        $form->onSuccess[] = $this->editFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function editFormSuccess (Form $form, ArrayHash $values)
    {
        $id = $values->id;
        unset($values->id);

        $ok = $this->model->editCourse($id, $values);
        if ($ok) {
            $this->flashMessage('Úprava záznamu proběhla úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Úprava záznamu selhala.', 'danger');
        }
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    private function CourseForm ()
    {
        $form = new Form();

        $form->addText('nazev', 'Jméno kurzu:')
            ->setRequired('Vložte jméno kurzu.');

        $form->addText('uroven', 'Úroveň:')
            ->setRequired('Zadejte úroveň kurzu.')
            ->setType('number')
            ->addRule(Form::INTEGER, 'Úroveň musí být celé číslo.')
            ->addRule(Form::RANGE, 'Úroveň musí být v rozsahu 1 - 10.', array (1, 10));

        $form->addText('obtiznost', 'Obtížnost:')
            ->setRequired('Zadejte obtížnost kurzu.');

        $form->addText('cena', 'Cena:')
            ->setRequired('Zadejte cenu kurzu.')
            ->addRule(Form::INTEGER, 'Cena musí být celé číslo.')
            ->addRule(FORM::MAX_LENGTH, 'Maximální délka ceny musí být 12 znaků', 12);

        $form->addText('popis', 'Popis:');

        $form->addCheckboxList('prerek', 'Prerekvizity:', $this->model->getCourses()->fetchPairs('id', 'nazev'));

        $form->addSubmit('submit', 'Uložit');

        return $form;
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentAddForm ()
    {
        $form = $this->CourseForm();

        $form->onSuccess[] = $this->addFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function addFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->model->addCourse($values);
        if ($ok) {
            $this->flashMessage('Vložení záznamu proběhlo úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Vložení záznamu selhalo.', 'danger');
        }
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentOrderForm ()
    {
        // Spojení jednotlivých částí řádku z databáze do jednoho řetězce v poli
        $rooms = array ();
        foreach ($this->roomModel->getRooms() as $row) {
            $rooms[$row->id] = $row->kapacita . " míst, " . $row->adresa . ', ' . $row->cena . ' Kč';
        }

        $form = new Form();

        $form->addRadioList('misto', 'Místnosti:', $rooms)
            ->setRequired('Vyberte Místnost pro konání kurzu.')
            ->setDefaultValue(1);

        $form->addSelect('vede', 'Lektor:');

        $form->addText('kapacita', 'Kapacita:')
            ->setRequired('Musíte zadat kapacitu kurzu.')
            ->setType('number')
            ->addRule(FORM::INTEGER, 'Kapacita musí být celé číslo.')
            ->addRule(FORM::MIN, 'Kapacita musí být větší jak 0.', 1);

        $form->addHidden('kurz');

        $form->addSubmit('submit', 'Objednat');

        $form->onValidate[] = $this->courseFormValidate;
        $form->onSuccess[] = $this->orderFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     *
     * @return bool
     */
    public function courseFormValidate (Form $form)
    {
        $values = $form->getValues();

        $room = $this->roomModel->getRoom($values->misto);

        if ($values->kapacita > $room->kapacita) {
            $form['kapacita']->addError('Tam se nevejdete. Ve vybrané místnosti je ' . $room->kapacita . ' míst.');

            return false;
        }

        return true;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function orderFormSuccess (Form $form, ArrayHash $values)
    {
        $values->firma = $this->user->identity->data['ico'];

        $ok = $this->model->orderCourse($values);
        if ($ok) {
            $this->flashMessage('Objednání kurzu proběhlo úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Objednání kurzu selhalo.', 'danger');
        }
    }

    /**
     * @param int $id
     */
    public function actionEditOrdered ($id)
    {
        $course = $this->model->getOrderedCourse($id);
        if (!$course) {
            $this->flashMessage('Kurz nebyl nalezen.', 'danger');
            $this->redirect('Course:listOrdered');
        }

        $this['orderedForm']['kurz']->setValue($id);
        $this['orderedForm']['misto']
            ->setItems($this->roomModel->getRooms()->fetchPairs('id', 'adresa'));
        $this['orderedForm']->setDefaults($course);
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentOrderedForm ()
    {
        $form = new Form;

        $form->addHidden('kurz');

        $form->addSelect('misto', 'Místnost')
            ->setRequired();

        $form->addText('kapacita', 'Kapacita')
            ->setType('number')
            ->setRequired();

        $form->addSubmit('edit', 'Upravit');

        $form->onValidate[] = $this->courseFormValidate;
        $form->onSuccess[] = $this->orderedFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function orderedFormSuccess (Form $form, ArrayHash $values)
    {
        $kurz = $values->kurz;
        unset($values->kurz);

        $ok = $this->model->editOrderedCourse($kurz, $values);
        if ($ok) {
            $this->flashMessage('Editace proběhla úspěšně.', 'success');
            $this->redirect('Course:listOrdered');
        } else {
            $this->flashMessage('Editace skončila chybou nebo jste nezměnili žádnou položku.', 'danger');
        }
    }
}
