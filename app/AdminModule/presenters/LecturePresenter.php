<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 15:09
 */

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\AdminForm as Form;
use Nette\Utils\ArrayHash;
use Vodacek\Forms\Controls\DateInput;

class LecturePresenter extends BasePresenter
{

    /**
     * @inject
     * @var \App\AdminModule\model\LectureModel
     */
    public $model;

    /**
     * @inject
     * @var \App\AdminModule\model\CourseModel
     */
    public $courseModel;

    /**
     * @param int $id
     */
    public function actionList ($id)
    {
        $course = $this->courseModel->getCourse($id);
        if (!$course) {
            $this->flashMessage('Kurz nebyl nalezen', 'danger');
            $this->redirect('Course:list');
        }

        $this->template->course = $course;

        $this->template->lectures = $this->model->getLectures($id);
    }

    /**
     * @param int $id id lekce
     */
    public function actionEdit ($id)
    {
        $defaults = $this->model->getLecture($id);
        if (!$defaults) {
            $this->flashMessage('Záznam nebyl nalezen', 'danger');
            $this->redirect('list');
        }

        $this['editForm']['kurz']->setItems($this->courseModel->getCourses()->fetchPairs('id', 'nazev'));
        $this['editForm']->setDefaults($defaults->toArray());
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentEditForm ()
    {
        $form = $this->LectureForm();

        $form->addHidden('id');

        $form->onSuccess[] = $this->editFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function editFormSuccess (Form $form, ArrayHash $values)
    {
        $id = $values->id;
        unset($values->id);

        $ok = $this->model->editLecture($id, $values);
        if ($ok) {
            $this->flashMessage('Úprava záznamu proběhla úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Úprava záznamu selhala.', 'danger');
        }
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    private function LectureForm ()
    {
        /**
         * názvy polí ve formuláři dávej stejný jako jméno sloupce v databázi
         */
        $form = new Form();

        $form->addText('nazev', 'Jméno lekce:')
            ->setRequired('Vložte jméno lekce.');

        $form->addText('popis', 'Popis:');

        $form->addSelect('kurz', 'Kurz:');

        $form->addSubmit('submit', 'Uložit');

        return $form;
    }

    public function actionAdd ()
    {
        $this['addForm']['kurz']->setItems($this->courseModel->getCourses()->fetchPairs('id', 'nazev'));
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentAddForm ()
    {
        $form = $this->LectureForm();

        $form->onSuccess[] = $this->addFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function addFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->model->addLecture($values);
        if ($ok) {
            $this->flashMessage('Vložení záznamu proběhlo úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Vložení záznamu selhalo.', 'danger');
        }
    }

    /**
     * @param int $id id lekce
     *
     * @Allowed(resource=Lecture, privilege=order)
     */
    public function actionOrder ($id)
    {
        $defaults = $this->model->getLecture($id);
        if (!$defaults) {
            $this->flashMessage('Záznam nebyl nalezen', 'danger');
            $this->redirect('list');
        }

        $this['orderForm']['lekce']->setValue($id);
        $this['orderForm']['kurz']->setValue($defaults->kurz);
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentOrderForm ()
    {
        $form = new Form();

        $form->addHidden('lekce');
        $form->addHidden('kurz');

        $dateInput = new DateInput('Datum:');
        $dateInput->setRequired('Zadejte datum.')
            ->addRule(Form::MIN, 'Čas konání lekce nemůže být dřívější než aktuální čas.', new \DateTime());
        $form->addComponent($dateInput, 'datum');


        $form->addSubmit('submit', 'Objednat');

        $form->onSuccess[] = $this->orderFormSuccess;

        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function orderFormSuccess (Form $form, ArrayHash $values)
    {
        $ok = $this->model->orderLecture($values);
        if ($ok) {
            $this->flashMessage('Objednání kurzu proběhlo úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Objednání kurzu selhalo.', 'danger');
        }
    }
}
