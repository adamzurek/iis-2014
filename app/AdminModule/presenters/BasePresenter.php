<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 31. 10. 2014
 * Time: 11:07
 */

namespace App\AdminModule\Presenters;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \Nette\Application\UI\Presenter
{

    /**
     * @inject
     * @var \App\Components\IMenuControlFactory
     */
    public $menuControlFactory;

    /**
     * @return \App\Components\MenuControl
     */
    public function createComponentMenu ()
    {
        return $this->menuControlFactory->create();
    }

    /** @var \WebLoader\Nette\LoaderFactory @inject */
    public $webLoader;


    /** @return \WebLoader\Nette\CssLoader */
    protected function createComponentCss()
    {
        return $this->webLoader->createCssLoader('default');
    }

    /** @return \WebLoader\Nette\JavaScriptLoader */
    protected function createComponentJs()
    {
        return $this->webLoader->createJavaScriptLoader('default');
    }

    /**
     * Nastaví title stránky "Administrace" + co je v konfiguráku pod parametrem "siteName"
     */
    public function beforeRender() {
        $this->template->siteName = 'Administrace ' . (isset($this->context->parameters['siteName']) ? $this->context->parameters['siteName'] : 'Školicí středisko');

        $this['menu']->brand = 'Administrace';
        $this['menu']
                ->addItem('Room:add', 'Přidat místnost')
                ->addItem('Course:list', 'Kurzy')
                ->addItem('Course:listOrdered', 'Kurzy')
                ->addItem('Attendee:edit', 'Změnit údaje')
                ->addItem('Sign:out', 'Odhlásit', $this['menu']::TYPE_RIGHT);
    }

    public function startup() {
        parent::startup();

        $this->user->getStorage()->setNamespace('iis14/back');
        $this->user->setAuthorizator(new \App\Model\UserAuthorizator);

        if ($this->name != 'Admin:Sign' && !$this->user->loggedIn) {

            $this->redirect('Sign:in', array('backlink' => $this->storeRequest()));
        }
    }

    /**
     * @var var $element
     * @forwards Homepage:|Error:blank
     */
    public function checkRequirements($element) {
        $this->startup();

        $class = $element->name;
        $method = 'action' . $this->action;

        $signal = $this->getSignal();
        if ($signal) {
            if ($signal[0]) {
                $class = $this->getComponent($signal[0]);
            }
            $method = 'handle' . $signal[1];
        }

        try {
            $c = new \Nette\Reflection\ClassType($class);
            $m = new \Nette\Reflection\Method($class, $method);

            $fail = false;
            for ($i = $c; $i != $m && $fail != true; $i = $m) {
                if ($i->hasAnnotation('logged')) {
                    if (!$this->user->loggedIn) {
                        $this->flashMessage('Pro tuto akci musíte být přihlášen(a).', 'info');
                        $fail = true;
                    }
                } elseif ($i->hasAnnotation('only_guest')) {
                    if ($this->user->loggedIn) {
                        $this->flashMessage('Pro tuto akci nesmíte být přihlášen(a).', 'warning');
                        $fail = true;
                    }
                }

                if ($i->hasAnnotation('Allowed')) {
                    $allowed = $i->getAnnotation('Allowed');

                    $fail = !$this->user->isAllowed($allowed['resource'], $allowed['privilege']);

                    if ($fail) {
                        $this->flashMessage('Pro tuto akci nemáte oprávnění.', 'danger');
                    }
                }
            }

            if ($fail) {
                if ($this->isAjax()) {
                    $this->forward('Error:blank');
                } else {
                    $this->forward('Homepage:');
                }
            }
        } catch(\ReflectionException $e) {

        }
    }
}
