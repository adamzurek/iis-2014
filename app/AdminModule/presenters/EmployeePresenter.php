<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 29.11.2014
 * Time: 11:54
 */

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\AdminForm as Form;
use Nette\Utils\ArrayHash;

class EmployeePresenter  extends BasePresenter
{
    /**
     * @inject
     * @var \App\AdminModule\Model\EmployeeModel
     */
    public $model;

    /**
     * @inject
     * @var \App\AdminModule\Model\CourseModel
     */
    public $CourseModel;

    public function actionList()
    {
        $this->template->employees = $this->model->getEmployees();
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    private function EmployeeForm()
    {
        /**
         * názvy polí ve formuláři dávej stejný jako jméno sloupce v databázi
         */
        $form = new Form();

        $form->addText('rc', 'Rodné číslo:')
            ->setRequired('Vložte rodné číslo zaměstnance.')
            ->setType('number')
            ->addRule(Form::INTEGER, 'Rodné číslo musí být celé číslo.')
            ->addRule(FORM::MAX_LENGTH, 'Rodné číslo musí mít 10 čísel', 10)
            ->addRule(FORM::MIN_LENGTH, 'Rodné číslo musí mít 10 čísel', 10);

        $form->addText('jmeno', 'Jméno a příjmení:')
            ->setRequired('Vložte jméno a příjmení zaměstnance.');

        $form->addCheckboxList('vyskoleny', 'Vyškolený::', $this->CourseModel->getCourses()->fetchPairs('id', 'nazev'));

        return $form;
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentRegisterForm()
    {
        $form = $this->EmployeeForm();

        $form->onValidate[] = $this->registerFormValidate;
        $form->addSubmit('submit', 'Registrovat');

        $form->onSuccess[] = $this->registerFormSuccess;
        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm $form
     * @param \Nette\Utils\ArrayHash                $values
     */
    public function registerFormSuccess(Form $form, ArrayHash $values)
    {
        $ok = $this->model->addEmployee($values);
        if ($ok) {
            $this->flashMessage('Vložení záznamu proběhlo úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Vložení záznamu selhalo.', 'danger');
        }
    }

    /**
     * @param \App\Components\BootstrapForm
     *
     * @return bool
     */
    public function registerFormValidate(Form $form)
    {
        $values = $form->getValues();

        $human = $this->model->getEmployee($values->rc);

        if(fmod($values->rc, 11) != 0)
        {
            $form['rc']->addError('Rodné číslo je neplatné. Zkontrolujte si jeho správnost.');

            return false;
        }

        if($human)
        {
            $form['rc']->addError('Zaměstnanec s rodným číslem: ' . $human->rc . ' je u nás již registrován.');

            return false;
        }
        return true;
    }

    /**
     * @param int $rc rc zaměstnance
     */
    public function actionEdit($rc)
    {
        $defaults = $this->model->getEmployee($rc);
        if (!$defaults) {
            $this->flashMessage('Záznam nebyl nalezen', 'danger');
            $this->redirect('list');
        }

        unset($this['editForm']['rc']);
        $this['editForm']->addHidden('rc');
        $this['editForm']->setDefaults($defaults->toArray());
    }

    /**
     * @return \App\AdminModule\Components\AdminForm
     */
    public function createComponentEditForm()
    {
        $form = $this->EmployeeForm();

        $form->addSubmit('submit', 'Uložit');

        $form->onSuccess[] = $this->editFormSuccess;
        return $form;
    }

    /**
     * @param \App\AdminModule\Components\AdminForm|Form $form
     * @param ArrayHash|\Nette\Utils\ArrayHash $values
     */
    public function editFormSuccess(Form $form, ArrayHash $values)
    {
        $rc = $values->rc;
        unset($values->rc);

        $ok = $this->model->editEmployee($rc, $values);
        if ($ok) {
            $this->flashMessage('Úprava záznamu proběhla úspěšně.', 'success');
            $this->redirect('list');
        } else {
            $this->flashMessage('Úprava záznamu selhala.', 'danger');
        }
    }
} 