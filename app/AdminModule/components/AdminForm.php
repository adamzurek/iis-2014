<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 31. 10. 2014
 * Time: 11:21
 */

namespace App\AdminModule\Components;

class AdminForm extends \App\Components\BootstrapForm
{

    public function __construct($a = null, $b = null) {
        parent::__construct($a, $b);

        $this->addProtection('Vypršel bezpečnostní kód. Odešlete formulář znovu.');
    }

}
