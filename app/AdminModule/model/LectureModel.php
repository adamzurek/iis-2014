<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 14:53
 */

namespace App\AdminModule\Model;

class LectureModel extends \App\Model\LectureModel
{
    const
        TABLE_ORDERED_LECTURE = 'objednane_lekce';

    /**
     * Upraví lekce dle id
     *
     * @param int $lecture_id
     * @param array $data
     *
     * @return int
     */
    public function editLecture ($lecture_id, $data)
    {
        return $this->db->table(self::TABLE_LECTURE)->wherePrimary($lecture_id)->update($data);
    }

    /**
     * Vloží novou lekci
     *
     * @param array $data
     * @return mixed
     */
    public function addLecture ($data)
    {
        $checkState = $this->db->table(self::TABLE_LECTURE)->insert($data);

        if(!$checkState)
            return false;
        return true;
    }

    /**
     * Informace o konkrétní objednané lekci.
     *
     * @param int $id
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getOrderedLecture ($id)
    {
        return $this->db->table(self::TABLE_ORDERED_LECTURE)->wherePrimary($id)->fetch();
    }

    /**
     * @param $values
     *
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function orderLecture ($values)
    {
        return $this->db->table(self::TABLE_ORDERED_LECTURE)->insert($values);
    }
}
