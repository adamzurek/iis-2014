<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 8. 12. 2014
 * Time: 23:05
 */

namespace App\AdminModule\Model;


use Nette\Utils\ArrayHash;

class RoomModel extends \App\Model\RoomModel
{

    public function addRoom (ArrayHash $values)
    {
        return $this->db->table(self::TABLE_ROOM)->insert($values);
    }
}
