<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 29.11.2014
 * Time: 11:42
 */

namespace App\AdminModule\Model;

use Nette\Utils\ArrayHash,
    Nette\Utils\Strings;

class EmployeeModel extends \App\Model\EmployeeModel
{
    /**
     * Upraví zaměstnance dle $rc
     *
     * @param int $rc
     * @param \Nette\Utils\ArrayHash $data
     *
     * @return int
     */
    public function editEmployee ($rc, ArrayHash $data)
    {
        $employee = clone $data;

        unset($employee->vyskoleny);

        $this->db->table(self::TABLE_EMPLOYEE)->wherePrimary($rc)->update($employee);

        if(isset($data->vyskoleny))
        {
            // odstranit aktualni vyskoleni zamestnance
            $this->db->table(self::TABLE_EDUCATED)->where('rc', $rc)->delete();

            // vlozit zaskrtnute
            foreach($data->vyskoleny as $course_id)
            {
                $this->db->table(self::TABLE_EDUCATED)->insert(array('rc' => $rc, 'kurz' =>  $course_id));

                $checkState = $this->db->table(self::TABLE_EDUCATED)->where('rc', $rc)->where('kurz', $course_id);
                if(!$checkState) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Vloží nového zaměstnance
     *
     * @param ArrayHash $data
     * @return bool
     */
    public function addEmployee (ArrayHash $data)
    {
        $employee = clone $data;
        unset($employee->vyskoleny);

        $employee['heslo'] = Strings::random(40, '0-9a-z');

        $this->db->table(self::TABLE_EMPLOYEE)->insert($employee);
        $check = $this->db->table(self::TABLE_EMPLOYEE)->wherePrimary($data->rc);

        if($check)
        {
            if(isset($data->vyskoleny))
            {
                foreach($data->vyskoleny as $course_id)
                {
                    $this->db->table(self::TABLE_EDUCATED)->insert(array('rc' => $data->rc, 'kurz' =>  $course_id));
                    $checkState = $this->db->table(self::TABLE_EDUCATED)->where('rc', $data->rc)->where('kurz', $course_id);
                    if(!$checkState)
                        return false;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
} 