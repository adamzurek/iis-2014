<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 17:59
 */

namespace App\AdminModule\Model;


class CompanyModel extends \App\Model\CompanyModel
{
    /**
     * Upraví firmu dle id
     *
     * @param int $id
     * @param array $data
     *
     * @return int
     */
    public function editCompany ($id, $data)
    {
        return $this->db->table(self::TABLE_COMPANY)->wherePrimary($id)->update($data);
    }
} 