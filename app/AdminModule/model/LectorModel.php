<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 9.11.2014
 * Time: 14:43
 */

namespace App\AdminModule\Model;


use Nette\Database\SqlLiteral;

class LectorModel extends \App\Model\BaseModel
{
    const
        TABLE_EDUCATED = 'vyskoleni',
        TABLE_LECTOR = 'zamestnanec';

    /**
     * Seznam lektorů vyškolených pro daný kurz.
     *
     * @param int $course_id
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getLectors ($course_id)
    {
        return $this->db->query(
            "SELECT z.rc, v.kurz, z.jmeno FROM ? v INNER JOIN ? z ON v.rc = z.rc WHERE v.kurz = ?",
            new SqlLiteral(self::TABLE_EDUCATED), new SqlLiteral(self::TABLE_LECTOR), $course_id
        );
    }
}