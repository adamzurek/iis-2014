<?php
/**
 * Created by PhpStorm 8.0.1
 * User: Dreanux
 * Date: 28.11.2014
 * Time: 18:08
 */

namespace App\AdminModule\Model;


use Nette\Utils\ArrayHash;

class AttendeeModel extends \App\Model\AttendeeModel
{
    const
        TABLE_ENROLLED = 'prihlaseni';

    /**
     * Upraví účastníka dle id
     *
     * @param int                    $id
     * @param \Nette\Utils\ArrayHash $data
     *
     * @return bool
     */
    public function editAttendee ($role, $id, ArrayHash $data)
    {
        if ($data->zamestnan == 0) {
            $data->zamestnan = NULL;
        }

        if (empty($data->heslo)) {
            unset($data->heslo);
        } else {
            $data->heslo = sha1($data->heslo);
        }

        $this->db->table($role)->wherePrimary($id)->update($data);

        $row = $this->db->table($role)->wherePrimary($id)->fetch();

        return $row->zamestnan == $data->zamestnan && $row->jmeno == $data->jmeno;
    }

    /**
     * @param string $role
     * @param int $id
     *
     * @return bool|mixed|\Nette\Database\Table\IRow|\Nette\Database\Table\Selection
     */
    public function get ($role, $id)
    {
        return $this->db->table($role)->wherePrimary($id)->fetch();
    }

    /**
     * @param int $course_id
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getAttendeesByCourse ($course_id)
    {
        return $this->db->table(self::TABLE_ATTENDEE)->where(':' . self::TABLE_ENROLLED . '.kurz', $course_id);
    }
}
