<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 3. 11. 2014
 * Time: 16:55
 */

namespace App\AdminModule\Model;


use Nette\Utils\ArrayHash;

class CourseModel extends \App\Model\CourseModel
{

    /**
     * Vloží nový prováděný kurz
     *
     * @param \Nette\Utils\ArrayHash $data
     *
     * @return bool
     */
    public function addCourse (ArrayHash $data)
    {
        $provadene_kurzy = clone $data;
        unset($provadene_kurzy->prerek);

        $course_id = $this->db->table(self::TABLE_COURSE)->insert($provadene_kurzy);
        if ($course_id) {
            if (isset($data->prerek)) {
                foreach ($data->prerek as $row) {
                    $checkState = $this->db->table(self::TABLE_PREREQUISITE)->insert(array ('prerekvizita' => $row,
                                                                                            'kurz'         => $course_id
                    ));
                    if (!$checkState) {
                        return false;
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Upraví prováděný kurz dle id
     *
     * @param int                    $course_id
     * @param \Nette\Utils\ArrayHash $data
     *
     * @return int
     */
    public function editCourse ($course_id, ArrayHash $data)
    {
        return $this->db->table(self::TABLE_COURSE)->wherePrimary($course_id)->update($data);
    }

    /**
     * @param int                    $course_id
     * @param \Nette\Utils\ArrayHash $data
     *
     * @return int
     */
    public function editOrderedCourse ($course_id, ArrayHash $data)
    {
        return $this->db->table(self::TABLE_ORDERED_COURSE)->wherePrimary($course_id)->update($data);
    }

    /**
     * @param int|null $course_id
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getOrderedCourses ($course_id = null, $ico = null)
    {
        $courses = $this->db->table(self::TABLE_ORDERED_COURSE);

        if ($course_id) {
            $courses = $courses->where('kurz', $course_id);
        }

        if ($ico != -1) {
            $courses = $courses->where('firma', array (null, $ico));    // veřejné nebo od firmy
        }

        return $courses;
    }

    /**
     * @param string|int $ico
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getOrderedCoursesByCompany ($ico)
    {
        return $this->db->table(self::TABLE_ORDERED_COURSE)->where('firma', $ico);
    }

    /**
     * Informace o konkrétním objednaném kurzu.
     *
     * @param int $id
     *
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getOrderedCourse ($id)
    {
        return $this->db->table(self::TABLE_ORDERED_COURSE)->wherePrimary($id)->fetch();
    }

    /**
     * @param \Nette\Utils\ArrayHash $values
     *
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function orderCourse (ArrayHash $values)
    {
        return $this->db->table(self::TABLE_ORDERED_COURSE)->insert($values);
    }
}
