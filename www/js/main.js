$(document).ready(function() {
    $.nette.init();


    $('input[data-dateinput-type]').dateinput({
        datetime: {
            dateFormat: 'd.m.yy',
            timeFormat: 'H:mm',
            options: { // options for type=datetime
                changeYear: true
            }
        },
        'datetime-local': {
            dateFormat: 'd.m.yy',
            timeFormat: 'H:mm'
        },
        month: {
            dateFormat: 'MM yy'
        },
        week: {
            dateFormat: "w. 'week of' yy"
        },
        date: {
            dateFormat: 'd.m.yy'
        },
        time: {
            timeFormat: 'H:mm'
        },
        options: { // global options
            closeText: "Close"
        }
    });
});