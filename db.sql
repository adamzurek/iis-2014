-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vygenerováno: Čtv 30. říj 2014, 19:33
-- Verze MySQL: 5.6.19
-- Verze PHP: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `xproko30`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `firma`
--

CREATE TABLE IF NOT EXISTS `firma` (
  `ico` int(11) NOT NULL,
  `nazev` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `heslo` char(40) COLLATE utf8_czech_ci NOT NULL,
  `aktivni` tinyint(1) NOT NULL,
  PRIMARY KEY (`ico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `firma`
--

INSERT INTO `firma` (`ico`, `nazev`, `heslo`, `aktivni`) VALUES
(123456789, 'Velkopopovická Sodovka', sha1('krtekkokos'), 1),
(456123789, 'Hotel u Davida', sha1('5fw4s7d6w9r'), 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `lekce`
--

CREATE TABLE IF NOT EXISTS `lekce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `kurz` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kurz` (`kurz`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=5 ;

--
-- Vypisuji data pro tabulku `lekce`
--

INSERT INTO `lekce` (`id`, `nazev`, `popis`, `kurz`) VALUES
(1, 'Sebeovladani I', 'Naučte se ovládat své emoce.', 1),
(2, 'Sebeovladani II', 'Naučte se ovládat své emoce.', 2),
(3, 'Sebeovladani III', 'Naučte se ovládat své emoce.', 3),
(4, 'Diktatura', 'Staňte se správným diktátorem', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `mistnost`
--

CREATE TABLE IF NOT EXISTS `mistnost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kapacita` int(11) NOT NULL,
  `cena` decimal(10,2) NOT NULL,
  `adresa` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=5 ;

--
-- Vypisuji data pro tabulku `mistnost`
--

INSERT INTO `mistnost` (`id`, `kapacita`, `cena`, `adresa`) VALUES
(1, 50, '2000.00', 'Brno, Bezručova 10b'),
(2, 10, '1000.00', 'Brno, Bezručova 10b'),
(3, 60, '2500.00', 'Brno, Bezručova 10b'),
(4, 60, '10000.00', 'Kokolety, Kulová 666');

--
-- Spouště `mistnost`
--
DROP TRIGGER IF EXISTS `trigger_mistnost_insert`;
DELIMITER //
CREATE TRIGGER `trigger_mistnost_insert` BEFORE INSERT ON `mistnost`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF NEW.kapacita <= 0 THEN
    set msg = 'Error: Capacity must be bigger than 0.';
	signal sqlstate '45000' set message_text = msg;
ELSEIF NEW.cena < 0 THEN
	set msg = 'Error: Price must be positive number.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_mistnost_update`;
DELIMITER //
CREATE TRIGGER `trigger_mistnost_update` BEFORE UPDATE ON `mistnost`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF NEW.kapacita <= 0 THEN
    set msg = 'Error: Capacity must be bigger than 0.';
	signal sqlstate '45000' set message_text = msg;
ELSEIF NEW.cena < 0 THEN
	set msg = 'Error: Price must be positive number.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabulky `objednane_kurzy`
--

CREATE TABLE IF NOT EXISTS `objednane_kurzy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kapacita` int(11) NOT NULL,
  `vede` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `kurz` int(11) NOT NULL,
  `misto` int(11) NOT NULL,
  `firma` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kurz` (`kurz`),
  KEY `misto` (`misto`),
  KEY `firma` (`firma`),
  KEY `kapacita` (`kapacita`),
  KEY `vede` (`vede`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `objednane_kurzy`
--

INSERT INTO `objednane_kurzy` (`id`, `kapacita`, `vede`, `kurz`, `misto`, `firma`) VALUES
(1, 10, '9005049878', 1, 1, 123456789),
(2, 50, '9704030303', 2, 2, 456123789);

--
-- Spouště `objednane_kurzy`
--
DROP TRIGGER IF EXISTS `trigger_objkurzy_insert`;
DELIMITER //
CREATE TRIGGER `trigger_objkurzy_insert` BEFORE INSERT ON `objednane_kurzy`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF NEW.kapacita <= 0 THEN
    set msg = 'Error: Capacity must be bigger than 0.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_objkurzy_update`;
DELIMITER //
CREATE TRIGGER `trigger_objkurzy_update` BEFORE UPDATE ON `objednane_kurzy`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF NEW.kapacita <= 0 THEN
    set msg = 'Error: Capacity must be bigger than 0.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabulky `objednane_lekce`
--

CREATE TABLE IF NOT EXISTS `objednane_lekce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lekce` int(11) NOT NULL,
  `kurz` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lekce` (`lekce`),
  KEY `kurz` (`kurz`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `objednane_lekce`
--

INSERT INTO `objednane_lekce` (`id`, `datum`, `lekce`, `kurz`) VALUES
(1, '2014-11-08 09:45:00', 1, 1),
(2, '2014-04-20 11:00:00', 1, 1),
(3, '2014-04-20 12:00:00', 2, 2);

--
-- Spouště `objednane_lekce`
--
DROP TRIGGER IF EXISTS `trigger_objlekce_insert`;
DELIMITER //
CREATE TRIGGER `trigger_objlekce_insert` BEFORE INSERT ON `objednane_lekce`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF NEW.datum < NOW() THEN
    set msg = 'Error: Date cannot be earlier than current date.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_objlekce_update`;
DELIMITER //
CREATE TRIGGER `trigger_objlekce_update` BEFORE UPDATE ON `objednane_lekce`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF NEW.datum < NOW() THEN
    set msg = 'Error: Date cannot be earlier than current date.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabulky `prerekvizity`
--

CREATE TABLE IF NOT EXISTS `prerekvizity` (
  `prerekvizita` int(11) NOT NULL,
  `kurz` int(11) NOT NULL,
  KEY `kurz` (`kurz`),
  KEY `prerekvizita` (`prerekvizita`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `prerekvizity`
--

INSERT INTO `prerekvizity` (`prerekvizita`, `kurz`) VALUES
(1, 2),
(2, 3),
(1, 17),
(3, 17);

-- --------------------------------------------------------

--
-- Struktura tabulky `prihlaseni`
--

CREATE TABLE IF NOT EXISTS `prihlaseni` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `kurz` int(11) NOT NULL,
  KEY `kurz` (`kurz`),
  KEY `prihlaseni_ibfk_1` (`rc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `prihlaseni`
--

INSERT INTO `prihlaseni` (`rc`, `kurz`) VALUES
('9209156539', 1),
('9209156539', 2),
('9307161237', 2),
('9410283344', 1),
('9502173461', 1),
('9502173461', 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `provadene_kurzy`
--

CREATE TABLE IF NOT EXISTS `provadene_kurzy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `uroven` int(11) NOT NULL,
  `obtiznost` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `cena` decimal(10,2) NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=18 ;

--
-- Vypisuji data pro tabulku `provadene_kurzy`
--

INSERT INTO `provadene_kurzy` (`id`, `nazev`, `uroven`, `obtiznost`, `cena`, `popis`) VALUES
(1, 'Ovládni svůj vztek', 5, 'Obtížné', '2533.00', 'Naučte se, jak ovládnout svůj nahromaděný vztek proti svému protivníkovi.'),
(2, 'Nepodceňuj sílu vůle', 2, 'Středně pokročilý', '3500.00', 'Překonejte sami sebe!'),
(3, 'Dej si piškotek', 1, 'Pro mimina', '4750.00', NULL),
(4, 'Kokosy a cedníky', 4, 'Krutopřísně střední', '1500.00', 'Mango a kokos společně za dobrodružstvím.'),
(11, 'Jabber', 2, 'Ranní čaj', '560.00', ''),
(16, 'Folklórní žranice', 10, 'Silně devastující', '9560.00', 'Chcete zemřít? Pojďte sem.'),
(17, 'Gumování s Edou', 3, 'Lehké', '999.00', '');

--
-- Spouště `provadene_kurzy`
--
DROP TRIGGER IF EXISTS `trigger_prkurzy_update`;
DELIMITER //
CREATE TRIGGER `trigger_prkurzy_update` BEFORE UPDATE ON `provadene_kurzy`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF NEW.cena < 0 THEN
    set msg = 'Error: Price must be positive number.';
	signal sqlstate '45000' set message_text = msg;
ELSEIF NEW.uroven <= 0 OR NEW.uroven > 10 THEN
    set msg = 'Error: Level must be bigger than 0 and smaller than 11.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_prkuzy_insert`;
DELIMITER //
CREATE TRIGGER `trigger_prkuzy_insert` BEFORE INSERT ON `provadene_kurzy`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF NEW.cena < 0 THEN
    set msg = 'Error: Price must be positive number.';
	signal sqlstate '45000' set message_text = msg;
ELSEIF NEW.uroven <= 0 OR NEW.uroven > 10 THEN
    set msg = 'Error: Level must be bigger than 0 and smaller than 11.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabulky `ucastnik`
--

CREATE TABLE IF NOT EXISTS `ucastnik` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `adresa` varchar(60) COLLATE utf8_czech_ci DEFAULT NULL,
  `zamestnan` int(11) DEFAULT NULL,
  `heslo` char(40) COLLATE utf8_czech_ci NOT NULL,
  `aktivni` tinyint(1) NOT NULL,
  PRIMARY KEY (`rc`),
  KEY `ucastnik_ibfk_1` (`zamestnan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `ucastnik`
--

INSERT INTO `ucastnik` (`rc`, `jmeno`, `adresa`, `zamestnan`, `heslo`, `aktivni`) VALUES
('9209156539', 'Fiala Gordnová', 'Žďár nad Sázavou 53', NULL, sha1('s56d4f65sd4'), 1),
('9307161237', 'Martin Ferner', 'Kotvrdovice, Kotvrdovice 57', NULL, sha1('sdf46s5d4f6'), 1),
('9410283344', 'Ferdinand Cecil', 'Brno, Černovice 4b', NULL, sha1('65sd4f6sd4'), 1),
('9502173461', 'Martina Růžičková', 'Sebranice, Sebranice 7', NULL, sha1('56s4df6+5s4d'), 1);

--
-- Spouště `ucastnik`
--
DROP TRIGGER IF EXISTS `trigger_ucastnik_insert`;
DELIMITER //
CREATE TRIGGER `trigger_ucastnik_insert` BEFORE INSERT ON `ucastnik`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF MOD(NEW.rc, 11) != 0 THEN
    set msg = 'Error: RC is invalid. Check if you wrote it right.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_ucastnik_update`;
DELIMITER //
CREATE TRIGGER `trigger_ucastnik_update` BEFORE UPDATE ON `ucastnik`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF MOD(NEW.rc, 11) != 0 THEN
    set msg = 'Error: RC is invalid. Check if you wrote it right.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabulky `vyskoleni`
--

CREATE TABLE IF NOT EXISTS `vyskoleni` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `kurz` int(11) NOT NULL,
  KEY `kurz` (`kurz`),
  KEY `vyskoleni_ibfk_1` (`rc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `vyskoleni`
--

INSERT INTO `vyskoleni` (`rc`, `kurz`) VALUES
('9005049878', 1),
('9405303369', 1),
('9704030303', 1),
('9005049878', 2),
('9704030303', 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `zamestnanec`
--

CREATE TABLE IF NOT EXISTS `zamestnanec` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `heslo` char(40) COLLATE utf8_czech_ci NOT NULL,
  `aktivni` tinyint(1) NOT NULL,
  PRIMARY KEY (`rc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `zamestnanec`
--

INSERT INTO `zamestnanec` (`rc`, `jmeno`, `heslo`, `aktivni`) VALUES
('9005049878', 'Marek Kouřil', sha1('s65d4f65sd4f'), 1),
('9405303369', 'Karel Sedlák', sha1('s56d4f65sd4f'), 1),
('9704030303', 'Daniela Malá', sha1('s56d4f6s5d4f'), 1);

--
-- Struktura tabulky `stredisko`
--

CREATE TABLE IF NOT EXISTS `stredisko` (
  `rc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `heslo` char(40) COLLATE utf8_czech_ci NOT NULL,
  `aktivni` tinyint(1) NOT NULL,
  PRIMARY KEY (`rc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `stredisko`
--

INSERT INTO `stredisko` (`rc`, `jmeno`, `heslo`, `aktivni`) VALUES
('9005049878', 'Marek Kouřil', sha1('s65d4f65sd4f'), 1),
('9405303369', 'Karel Sedlák', sha1('s56d4f65sd4f'), 1),
('9704030303', 'Daniela Malá', sha1('s56d4f6s5d4f'), 1);

--
-- Spouště `zamestnanec`
--
DROP TRIGGER IF EXISTS `trigger_zamestnanec_insert`;
DELIMITER //
CREATE TRIGGER `trigger_zamestnanec_insert` BEFORE INSERT ON `zamestnanec`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF MOD(NEW.rc, 11) != 0 THEN
    set msg = 'Error: RC is invalid. Check if you wrote it right.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_zamestnanec_update`;
DELIMITER //
CREATE TRIGGER `trigger_zamestnanec_update` BEFORE UPDATE ON `zamestnanec`
 FOR EACH ROW BEGIN
declare msg varchar(255);
IF MOD(NEW.rc, 11) != 0 THEN
    set msg = 'Error: RC is invalid. Check if you wrote it right.';
	signal sqlstate '45000' set message_text = msg;
END IF;
END
//
DELIMITER ;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `lekce`
--
ALTER TABLE `lekce`
  ADD CONSTRAINT `lekce_ibfk_1` FOREIGN KEY (`kurz`) REFERENCES `provadene_kurzy` (`id`);

--
-- Omezení pro tabulku `objednane_kurzy`
--
ALTER TABLE `objednane_kurzy`
  ADD CONSTRAINT `objednane_kurzy_ibfk_1` FOREIGN KEY (`vede`) REFERENCES `zamestnanec` (`rc`) ON UPDATE CASCADE,
  ADD CONSTRAINT `objednane_kurzy_ibfk_2` FOREIGN KEY (`kurz`) REFERENCES `provadene_kurzy` (`id`),
  ADD CONSTRAINT `objednane_kurzy_ibfk_3` FOREIGN KEY (`misto`) REFERENCES `mistnost` (`id`),
  ADD CONSTRAINT `objednane_kurzy_ibfk_4` FOREIGN KEY (`firma`) REFERENCES `firma` (`ico`);

--
-- Omezení pro tabulku `objednane_lekce`
--
ALTER TABLE `objednane_lekce`
  ADD CONSTRAINT `objednane_lekce_ibfk_1` FOREIGN KEY (`lekce`) REFERENCES `lekce` (`id`),
  ADD CONSTRAINT `objednane_lekce_ibfk_2` FOREIGN KEY (`kurz`) REFERENCES `objednane_kurzy` (`id`);

--
-- Omezení pro tabulku `prerekvizity`
--
ALTER TABLE `prerekvizity`
  ADD CONSTRAINT `prerekvizity_ibfk_1` FOREIGN KEY (`kurz`) REFERENCES `provadene_kurzy` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `prerekvizity_ibfk_2` FOREIGN KEY (`prerekvizita`) REFERENCES `provadene_kurzy` (`id`) ON DELETE CASCADE;

--
-- Omezení pro tabulku `prihlaseni`
--
ALTER TABLE `prihlaseni`
  ADD CONSTRAINT `prihlaseni_ibfk_1` FOREIGN KEY (`rc`) REFERENCES `ucastnik` (`rc`) ON UPDATE CASCADE,
  ADD CONSTRAINT `prihlaseni_ibfk_2` FOREIGN KEY (`kurz`) REFERENCES `objednane_kurzy` (`id`);

--
-- Omezení pro tabulku `ucastnik`
--
ALTER TABLE `ucastnik`
  ADD CONSTRAINT `ucastnik_ibfk_1` FOREIGN KEY (`zamestnan`) REFERENCES `firma` (`ico`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omezení pro tabulku `vyskoleni`
--
ALTER TABLE `vyskoleni`
  ADD CONSTRAINT `vyskoleni_ibfk_1` FOREIGN KEY (`rc`) REFERENCES `zamestnanec` (`rc`) ON UPDATE CASCADE,
  ADD CONSTRAINT `vyskoleni_ibfk_2` FOREIGN KEY (`kurz`) REFERENCES `provadene_kurzy` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
